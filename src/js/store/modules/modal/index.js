import Vue from 'vue'
import Vuex from 'vuex'
import actions from './actions.js'
import mutations from './mutations.js'

export default {
    state: {
        name: '',
        data: {}
    },
    actions,
    mutations
}