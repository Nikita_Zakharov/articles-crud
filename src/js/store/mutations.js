export default {
    'SET_ARTICLES'(state, articles) {
        state.articles = articles
    },
    'SET_IS_AUTHORIZE'(state, isAuthorize) {
        state.isAuthorize = isAuthorize
    }
}