export default {
    'SET_MODAL_NAME'(state, name) {
        state.name = name
    },
    'SET_MODAL_DATA'(state, data) {
        state.data = data
    }
}