import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters.js'
import actions from './actions.js'
import mutations from './mutations.js'
import modal from './modules/modal/index'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        modal
    },
    state: {
        isAuthorize: false,
        articles: []
    },
    getters,
    actions,
    mutations
})