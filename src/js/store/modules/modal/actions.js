export default {
    openModal(store, {name, data}) {
        store.commit('SET_MODAL_NAME', name)
        store.commit('SET_MODAL_DATA', data)
    },
    closeModal(store, value) {
        store.commit('SET_MODAL_NAME', '')
        store.commit('SET_MODAL_DATA', {})
        return value
    }
}