import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../components/pages/Home.vue'
import Auth from '../components/pages/Auth/Auth.vue'
import Edit from "../components/pages/Edit.vue";

Vue.use(VueRouter)

export default new VueRouter({
    mode: 'history',
    base: '/',
    routes: [
        {
            path: '/',
            component: Home,
            name: 'home'
        },
        {
            path: '/auth',
            component: Auth,
            name: 'auth'
        },
        {
            path: '/edit',
            component: Edit,
            name: 'edit'
        }
    ]
})