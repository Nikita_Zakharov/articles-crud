export default {
    sortedArticlesByDate(store) {
        return store.articles.sort((a, b) => {
            const dateA = new Date(a.createdAt).getTime(),
                dateB = new Date(b.createdAt).getTime()

            return dateB - dateA;
        });
    }
}